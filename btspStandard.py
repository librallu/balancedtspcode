#!/usr/bin/python
from gurobipy import *
from sys import argv
from time import time
from itertools import combinations
from random import randint

epsilon = 1e-5
infty = 1e9

def evalSol(inst,s):
    res = 0
    r,n = inst['r'],inst['n']
    for i in range(n):
        res += r[ s[i], s[(i+1)%n] ]
    return res


def readInst(filename):
    with open(filename, 'r') as f:
        n,m = [ int (a) for a in f.readline().split(' ')]
        r = {}
        for l in f.readlines():
            u,v,w = l.split(' ')
            u,v,w = int(u),int(v),int(w)
            r[u,v] = w
            r[v,u] = w
        return {'n': n, 'm': m, 'r': r}


def subtourElim(m, where):
    if where == GRB.Callback.MIPSOL:
        vals = m.cbGetSolution(m._vars)
        selected = tuplelist((i,j) for i,j in m._vars.keys() if vals[i,j] > 1-epsilon )
        tour = subtour(selected,m._n)
        if len(tour) == m._n:
            print('tour: {}'.format(" ".join([str(i) for i in tour])))
            print('eval: {}'.format(evalSol(m._inst,tour)))
        if len(tour) < m._n:
            m.cbLazy(quicksum(m._vars[i,j] for i,j in itertools.combinations(tour,2) if (i,j) in m._vars.keys()) <= len(tour)-1)


def subtour(edges,n):
    unvisited = list(range(n))
    cycle = range(n+1)
    while unvisited:
        thiscycle = []
        neighbors = unvisited
        while neighbors:
            current = neighbors[0]
            thiscycle.append(current)
            unvisited.remove(current)
            neighbors = [j for i,j in edges.select(current,'*') if j in unvisited]
        if len(cycle) > len(thiscycle):
            cycle = thiscycle
    return cycle


def getWeight(r,u,v):
    if (u,v) in r:
        return r[u,v]
    else:
        return 0


def printColor(text, color="red"):
    print("\033[1;31m"+text+"\033[0;0m")


def run_cutting_plane(inst):
    n,r = inst['n'], inst['r']
    m = Model("btsp")
    printColor('\n'+'#'*15+' CONSTRUCTING MODEL '+'#'*15+'\n')

    # DATA VECTORS
    dist = { (i,j): r[i,j] for i,j in r.keys() }

    # VARIABLES
    x = m.addVars(dist.keys(), obj=dist, vtype=GRB.BINARY, name="x")
    for i,j in x.keys():
        x[j,i] = x[i,j]
    obj = m.addVar(name="obj", vtype=GRB.INTEGER)

    # DEGREE 2 CONSTRAINT
    m.addConstrs(x.sum(i,'*') == 2 for i in range(n))

    # OBJECTIVE VARIABLES
    m.addConstr( obj >= quicksum([ x[i,j]*dist[i,j]/2 for i,j in dist.keys() ]))
    m.addConstr( obj >= -quicksum([ x[i,j]*dist[i,j]/2 for i,j in dist.keys() ]))

    # OBJECTIVE
    m.setObjective(obj, GRB.MINIMIZE)
    m._vars = x
    m._n = n
    m.Params.lazyConstraints = 1
    m._inst = inst
    m.Params.MIPGapAbs = 0.
    m.Params.MIPGap = 0.
    m.params.FeasibilityTol = 1e-8

    printColor('\n'+'#'*15+' SOLVING MODEL '+'#'*15+'\n')
    m.presolve()
    m.optimize(subtourElim)

    vals = m.getAttr('x', x)
    selected = tuplelist((i,j) for i,j in vals.keys() if vals[i,j] >= 1-epsilon)
    tour = subtour(selected, n)
    assert len(tour) == n

    printColor('\ntour: {}'.format(" ".join([str(i) for i in tour])))
    printColor('eval: {}\n'.format(evalSol(inst,tour)))


if __name__ == "__main__":
    if len(argv) < 2:
        print("usage: {} FILENAME".format(argv[0]))
    else:
        t1 = time()
        inst = readInst(argv[1])
        run_cutting_plane(inst)
        t2 = time()-t1
        printColor('TOTAL RUNNING TIME (READ INSTANCES + SOLVING) : {}'.format(t2))