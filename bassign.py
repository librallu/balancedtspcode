#!/usr/bin/python
from gurobipy import *
from sys import argv
from time import time
from itertools import combinations
from random import randint
from time import time

epsilon = 1e-8
infty = 1e9

time_start = time()

ub_list = [] # contains (ubvalue, time)
lb_list = [] # contains (lbvalue, time)

def evalSol(inst,s):
    res = 0
    r,n = inst['r'],inst['n']
    for i in range(n):
        res += r[ s[i], s[(i+1)%n] ]
    return res


def readInst(filename):
    with open(filename, 'r') as f:
        n,m = [ int (a) for a in f.readline().split(' ')]
        r = {}
        for l in f.readlines():
            u,v,w = l.split(' ')
            u,v,w = int(u),int(v),int(w)
            r[u,v] = w
            r[v,u] = w
        return {'n': n, 'm': m, 'r': r}


def getWeight(r,u,v):
    if (u,v) in r:
        return r[u,v]
    else:
        return 0


def printColor(text, color="red"):
    print("\033[1;31m"+text+"\033[0;0m")


def getCurve(m, where):
    if where == GRB.Callback.MIPSOL:
        eval_tour = 0
        ub_list.append([abs(eval_tour), time()-time_start])


def run_cutting_plane(inst):
    n,r = inst['n'], inst['r']
    m = Model("btsp")
    printColor('\n'+'#'*15+' CONSTRUCTING MODEL '+'#'*15+'\n')

    factor = 100000

    # DATA VECTORS
    dist = { (i,j): r[i,j] for i,j in r.keys() }
    dist2 = { e: int( dist[e]/factor )+(dist[e]<0) for e in dist.keys() }
    dist3 = { e: dist[e]-factor*dist2[e] for e in dist.keys() }

    bound_obj2 = max(abs(min(dist2.values())), max(dist2.values()))
    bound_obj3 = max(abs(min(dist3.values())), max(dist3.values()))

    # VARIABLES
    x = m.addVars(dist.keys(), obj=dist, vtype=GRB.BINARY, name="x")
    for i,j in x.keys():
        x[j,i] = x[i,j]
    obj = m.addVar(name="obj", vtype=GRB.INTEGER)
    obj2 = m.addVar(name="obj2",lb=-bound_obj2*n, ub=bound_obj2*n, vtype=GRB.INTEGER)
    obj3 = m.addVar(name="obj3",lb=-bound_obj3*n, ub=bound_obj3*n, vtype=GRB.INTEGER)

    # DEGREE 2 CONSTRAINT
    m.addConstrs(x.sum(i,'*') == 2 for i in range(n))

    # OBJECTIVE VARIABLES
    m.addConstr( obj2 == quicksum([ x[i,j]*dist2[i,j] for i,j in dist.keys() ]), name="obj2")
    m.addConstr( obj3 == quicksum([ x[i,j]*dist3[i,j] for i,j in dist.keys() ]), name="obj3")
    m.addConstr( obj >= (obj2*factor+obj3)/2, name="obj_a")
    m.addConstr( obj >= -(obj2*factor+obj3)/2, name="obj_b")

    # OBJECTIVE
    m.setObjective(obj, GRB.MINIMIZE)
    m._vars = x
    m._n = n
    m._inst = inst
    m.Params.lazyConstraints = 1
    m.Params.MIPGapAbs = 0.
    m.Params.MIPGap = 0.
    m.Params.MIPFocus = 1

    printColor('\n'+'#'*15+' SOLVING MODEL '+'#'*15+'\n')
    m.presolve()
    m.optimize(getCurve)


if __name__ == "__main__":
    if len(argv) < 3:
        print("usage: {} FILENAME TIMELIMIT".format(argv[0]))
    else:
        t1 = time()
        timelimit = float(argv[2])
        inst = readInst(argv[1])
        run_cutting_plane(inst, timelimit)
        t2 = time()-t1
        printColor('TOTAL RUNNING TIME (READ INSTANCES + SOLVING) : {}'.format(t2))
        print(lb_list)
        print(ub_list)
        inst_name = argv[1].split("/")[-1].split(".")[0]
        with open("curves/bassign_{}".format(inst_name), "w") as f:
            last_val = -1
            for a,b in ub_list:
                if last_val > 0:
                    f.write("{}\t{}\n".format(b,last_val))
                f.write("{}\t{}\n".format(b,a)) 
                last_val = a

        