Code for the Balanced TSP Metaheuristic Summer School competition.

 - btsp.py: split model. Solves instances in a few seconds
 - btspStandard.py: naive model for the balanced TSP. I used it to show the difference between the two models.
 - Btsp/ instances for the competition (original version can be found here: http://195.201.24.233/mess2018/home.html)
 

# usage

With gurobi and the guroby python binding installed, run:

python2 btsp.py INSTANCE (ex: python2 btsp.py Btsp/0010.txt)
python btspStandard.py INSTANCE
