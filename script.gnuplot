set term pdf
set output "out.pdf"
set xlabel "time(s))"
set ylabel "Objective value"
set logscale y
plot "curves/btsp_0500" u 1:2 w lp title "objective", "curves/lb_btsp_0500" u 1:2 dt 2 w lp title "dual bound"
