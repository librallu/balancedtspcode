#!/usr/bin/python
from gurobipy import *
from sys import argv
from time import time
from itertools import combinations
from random import randint
from time import time

epsilon = 1e-8
infty = 1e9

time_start = time()

ub_list = [] # contains (ubvalue, time)
lb_list = [] # contains (lbvalue, time)


def readInst(filename):
    with open(filename, 'r') as f:
        n,m = [ int (a) for a in f.readline().split(' ')]
        r = {}
        for l in f.readlines():
            u,v,w = l.split(' ')
            u,v,w = int(u),int(v),int(w)
            r[u,v] = w
            r[v,u] = w
        return {'n': n, 'm': m, 'r': r}


def subtourElim(m, where):
    if where == GRB.Callback.SIMPLEX:
        print(m.cbGet(GRB.Callback.SPX_DUALINF))
    if where == GRB.Callback.MIPSOL:
        vals = m.cbGetSolution(m._vars)
        selected = tuplelist((i,j) for i,j in m._vars.keys() if vals[i,j] > 1-epsilon )
        tour = subtour(selected,m._n)
        if len(tour) < m._n:
            m.cbLazy(quicksum(m._vars[i,j] for i,j in itertools.combinations(tour,2) if (i,j) in m._vars.keys()) <= len(tour)-1)


def subtour(edges,n):
    unvisited = list(range(n))
    cycle = range(n+1)
    while unvisited:
        thiscycle = []
        neighbors = unvisited
        while neighbors:
            current = neighbors[0]
            thiscycle.append(current)
            unvisited.remove(current)
            neighbors = [j for i,j in edges.select(current,'*') if j in unvisited]
        if len(cycle) > len(thiscycle):
            cycle = thiscycle
    return cycle


def getWeight(r,u,v):
    if (u,v) in r:
        return r[u,v]
    else:
        return 0


def printColor(text, color="red"):
    print("\033[1;31m"+text+"\033[0;0m")


def run_cutting_plane(inst, timelimit):
    n,r = inst['n'], inst['r']
    m = Model("btsp")
    printColor('\n'+'#'*15+' CONSTRUCTING MODEL '+'#'*15+'\n')

    factor = 100000

    # DATA VECTORS
    dist = { (i,j): r[i,j] for i,j in r.keys() }
    dist2 = { e: int( dist[e]/factor )+(dist[e]<0) for e in dist.keys() }
    dist3 = { e: dist[e]-factor*dist2[e] for e in dist.keys() }

    bound_obj2 = max(abs(min(dist2.values())), max(dist2.values()))
    bound_obj3 = max(abs(min(dist3.values())), max(dist3.values()))

    # VARIABLES
    x = m.addVars(dist.keys(), obj=dist, vtype=GRB.BINARY, name="x")
    for i,j in x.keys():
        x[j,i] = x[i,j]
    obj = m.addVar(name="obj", vtype=GRB.INTEGER)
    obj2 = m.addVar(name="obj2",lb=-bound_obj2*n, ub=bound_obj2*n, vtype=GRB.INTEGER)
    obj3 = m.addVar(name="obj3",lb=-bound_obj3*n, ub=bound_obj3*n, vtype=GRB.INTEGER)

    # DEGREE 2 CONSTRAINT
    m.addConstr(x.sum('*') == 2*(n-1))

    # OBJECTIVE VARIABLES
    m.addConstr( obj2 == quicksum([ x[i,j]*dist2[i,j] for i,j in dist.keys() ]), name="obj2")
    m.addConstr( obj3 == quicksum([ x[i,j]*dist3[i,j] for i,j in dist.keys() ]), name="obj3")
    m.addConstr( obj >= (obj2*factor+obj3)/2, name="obj_a")
    m.addConstr( obj >= -(obj2*factor+obj3)/2, name="obj_b")

    # OBJECTIVE
    m.setObjective(obj, GRB.MINIMIZE)
    m.Params.TimeLimit = timelimit
    m._vars = x
    m._n = n
    m.Params.lazyConstraints = 1
    m._inst = inst
    m.Params.MIPGapAbs = 0.
    m.Params.MIPGap = 0.
    m.Params.MIPFocus = 1

    printColor('\n'+'#'*15+' SOLVING MODEL '+'#'*15+'\n')
    m.presolve()
    m.optimize(subtourElim)



if __name__ == "__main__":
    if len(argv) < 3:
        print("usage: {} FILENAME TIMELIMIT".format(argv[0]))
    else:
        t1 = time()
        timelimit = float(argv[2])
        inst = readInst(argv[1])
        run_cutting_plane(inst, timelimit)
        t2 = time()-t1
        printColor('TOTAL RUNNING TIME (READ INSTANCES + SOLVING) : {}'.format(t2))

        